USE [Gmc_Job]
GO
SET IDENTITY_INSERT [dbo].[Jobs] ON 

INSERT [dbo].[Jobs] ([Id], [IsActive], [CreatedDate], [LastModifiedDate], [DeletedDate], [CreatedBy], [LastModifiedBy], [DeletedBy], [Code], [Name], [Schedule], [Hour], [Minute], [Day], [DayOfWeek], [Month], [Interval], [IsEnabled], [OrganizationId]) VALUES (1, 1, CAST(N'2018-08-23T05:03:36.0978462+00:00' AS DateTimeOffset), CAST(N'2019-01-23T08:30:25.6346078+00:00' AS DateTimeOffset), NULL, NULL, 8623, NULL, N'import-employee', N'Import Employee', 3, 0, 0, NULL, NULL, NULL, NULL, 1, 1)
INSERT [dbo].[Jobs] ([Id], [IsActive], [CreatedDate], [LastModifiedDate], [DeletedDate], [CreatedBy], [LastModifiedBy], [DeletedBy], [Code], [Name], [Schedule], [Hour], [Minute], [Day], [DayOfWeek], [Month], [Interval], [IsEnabled], [OrganizationId]) VALUES (2, 1, CAST(N'2018-08-23T05:03:36.0979274+00:00' AS DateTimeOffset), CAST(N'2019-01-23T08:29:55.3237640+00:00' AS DateTimeOffset), NULL, NULL, 8623, NULL, N'import-actual-data', N'Import Actual Data', 3, 0, 0, NULL, NULL, NULL, NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[Jobs] OFF
