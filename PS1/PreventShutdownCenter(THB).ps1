 #/// <summary>
#/// Paging component
#/// </summary>
#/// <remarks>
#/// Author: TRAN HUY BAO
#/// Created: 8/3/2019
#// reused: high
# close browser first to execute
# cmd /c "shutdown /a"
$form = Get-Process |where {$_.mainWindowTItle -like "*Software Center*"}
$setPOS = @'
[DllImport("user32.dll")]
public static extern bool SetWindowPos(IntPtr hWnd,
IntPtr hWndInsertAfter,
int X,
int Y,
int cx,
int cy,
uint uFlags);
'@
$handle = $form.MainWindowHandle
$SetWindowPos = Add-Type -MemberDefinition $setPOS -name WinApiCall -passthru
$SetWindowPos::SetWindowPos($handle,-1,0,0,0,0,0x0080) # 0x0080 = SWP_HIDEWINDOW
Stop-Service -Name "CcmExec" -Force
Set-Service -Name "CcmExec" -StartupType Manual
pause 