param (
   [string]$ntid = ""
)
function certValidater($userNTID){
# Validate current user and his certificates for possible VSCs deployment
# Created by Gernot Adam CI/GS - gernot.adam@de.bosch.com
# Version 1.5
# Variables
[datetime]$date = get-date -Format 'yyyy-MM-dd HH:mm:ss' -ErrorAction Stop
[string]$BoschCA1DeDn = "CN=Bosch-CA1-DE, CN=PKI, DC=Bosch, DC=com"  # As PowerShell shows it (incl. spaces)
[int32]$SigningCertKeyUsage = 0x80
[int32]$EncryptionCertKeyUsage = 0x20
[string]$EvtxLogName = "Application"
[string]$SmimeCertOid = "1.3.6.1.5.5.7.3.4"
[string]$SmartCardLogonOid = "1.3.6.1.4.1.311.20.2.2"
[int32]$IsCertValid = 0
[string]$CertInvalidMessage = ""
[string]$DistinguishedName = ""

try {
	# Open Cert-Store
	$CertStore = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Store("My","CurrentUser") -ErrorAction Stop
	$CertStore.Open("OpenExistingOnly,ReadOnly")

	# Start Certificate Validation
	#if ($CertStore.Certificates.Count -ne 0) {
		Write-Host ("Local Certificate Store Counter: {0}" -f $CertStore.Certificates.Count.ToString())
		$ValidCerts = $CertStore.Certificates.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByTimeValid,$date,0)
		$ValidBoschCerts = $ValidCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByIssuerDistinguishedName,$BoschCA1DeDn,0)
		$SmimeBoschCerts = $ValidBoschCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByApplicationPolicy,$SmimeCertOid,0)
		$SmimeEncryptionCert = $SmimeBoschCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByKeyUsage,$EncryptionCertKeyUsage,0)
		$SmimeSigningCert = $SmimeBoschCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByKeyUsage,$SigningCertKeyUsage,0)
		$SmcSigningCerts = $ValidCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByApplicationPolicy,$SmartCardLogonOid,0)
		
		# Find current end-user DN via Signing Cert
		if (($SmimeBoschCerts[0].Subject.Length -ne 0) -and ($SmcSigningCerts.Count -ne 0)) {
			$SmcSigningUserCert = $SmcSigningCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindBySubjectDistinguishedName,$SmimeBoschCerts[0].Subject.ToString(),0)        
		}
		# Local validation
		if (($SmcSigningUserCert.Count -ne 0) -and ($SmcSigningUserCert.HasPrivateKey -eq $true)) {
			#Write-Host ("User has already a valid local VirtualSmartCard certificate.")
            #$CertInvalidMessage = "User has already a valid local VirtualSmartCard certificate."
			$DistinguishedName = $SmcSigningUserCert.Subject.ToString()
            $IsCertValid = 1
		}
		else {
			# Start full validation.
			# Get required information for validation
			Write-Host ("User has no local valid local VirtualSmartCard certificate.")
			if ($SmimeBoschCerts.Count -ge 0) {
				try {
					Write-Host ("SMIME Certificate Counter is: {0}" -f $SmimeBoschCerts.Count)
					 
					# Get Current AD User UPN
					Add-Type -AssemblyName System.DirectoryServices.AccountManagement -ErrorAction Stop
					[string]$CurrentUpn =$userNTID + "@bosch.com" #[System.DirectoryServices.AccountManagement.UserPrincipal]::Current.UserPrincipalName #"tfw1fe@bosch.com""dnv1hc@bosch.com" MAZ2BP iwt8fe
					
					[string]$PkiLdapSrv = "rb-pkildap-standard:50000"
					[string]$PkiLdapBaseDN = "cn=bosch,cn=pki,dc=bosch,dc=com"
					$PkiLdapConnectionTimeOut = New-Object -TypeName System.TimeSpan(0,1,30) -ErrorAction Stop
					$PkiLdapSearchTimeOut = New-Object -TypeName System.TimeSpan(0,0,60) -ErrorAction Stop
					
					# Get Current PKI LDAP User Information
					# Needs reference to .NET assembly used in the script.
					Add-Type -AssemblyName System.DirectoryServices.Protocols -ErrorAction Stop
					
					# Create LDAP connector
					$LdapConnection = New-Object -TypeName System.DirectoryServices.Protocols.LdapConnection -ArgumentList $PkiLdapSrv -ErrorAction Continue
					
					# LDAP/S - True/False
					$LdapConnection.SessionOptions.SecureSocketLayer = $False
					# Authentication type: Anonymous, Basic, Digest, DPA (Distributed Password Authentication), External, Kerberos, Msn, Negotiate, Ntlm, Sicily 
					$LdapConnection.AuthType = [System.DirectoryServices.Protocols.AuthType]::Anonymous
					$LdapConnection.Timeout = $PkiLdapConnectionTimeOut
	
					# Set LDAP Search parameters
					$LdapSearch = New-Object System.DirectoryServices.Protocols.SearchRequest -ErrorAction Stop	
					$LdapSearch.DistinguishedName = $PkiLdapBaseDN
					$LdapSearch.Scope = "Subtree"
					$LdapSearch.SizeLimit = 100
					$LdapSearch.TimeLimit = $PkiLdapSearchTimeOut
					$LdapSearch.Aliases = "Never"
					[void]$LdapSearch.Attributes.Add("distinguishedName")
					[void]$LdapSearch.Attributes.Add("mail")
					[void]$LdapSearch.Attributes.Add("userCertificate")
					[void]$LdapSearch.Attributes.Add("userPrincipalName")
	
					# Defining the object type returned from searches for performance reasons.
					[System.DirectoryServices.Protocols.SearchResponse]$LdapResponse = $null
					$LdapFilter = "(&(objectClass=user)(userPrincipalName=$CurrentUpn))"
					$LdapSearch.Filter = $LdapFilter 
					
					if ($CurrentUpn.Length -ne 0) {
						try {
							# Get information from PKI LDAP for validation
							$CertCol = New-Object Security.Cryptography.X509Certificates.X509Certificate2Collection -ErrorAction Stop
							$LdapResponse = $LdapConnection.SendRequest($LdapSearch)
							Write-Host ("LdapServer: {0} connected with search filter: {1}" -f $PkiLdapSrv, $LdapFilter.ToString())
							if ($LdapResponse.Entries.Count -ne 0) {
								[int32]$count = 0
								Write-Host ("LDAP Certificate Counter: {0}" -f $LdapResponse.Entries.Attributes.usercertificate.Count)
								while ($count -le ($LdapResponse.Entries.Attributes.usercertificate.Count - 1)) {
									[void]$CertCol.Import($LdapResponse.Entries.Attributes.usercertificate.Item($count))
									$count++
								}
								[string]$LdapDistinguishedName = $LdapResponse.Entries.Attributes.distinguishedname.Item(0).ToString()
								[string]$LdapMail = $LdapResponse.Entries.Attributes.mail.Item(0).ToString()
								[string]$LdapUpn = $LdapResponse.Entries.Attributes.userprincipalname.Item(0).ToString()
								
								Write-Host ("UPN from LDAP is: {0}`r`n" -f $LdapUpn)
								
								$LdapValidCerts = $CertCol.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByTimeValid,$date,0)
								$LdapValidBoschCerts = $LdapValidCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByIssuerDistinguishedName,$BoschCA1DeDn,0)
								$LdapSmimeBoschCerts = $LdapValidBoschCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByApplicationPolicy,$SmimeCertOid,0)
								$LdapBoschEncryptionCert = $LdapSmimeBoschCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByKeyUsage,$EncryptionCertKeyUsage,0)
								$LdapBoschSigningCert = $LdapSmimeBoschCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByKeyUsage,$SigningCertKeyUsage,0)
								$LdapBoschSmcSigningCert = $LdapValidBoschCerts.Find([System.Security.Cryptography.X509Certificates.X509FindType]::FindByApplicationPolicy,$SmartCardLogonOid,0)
							
								[array]$FoundSigningCerts = @($LdapBoschSigningCert)
								Write-Host ("Count of array is :{0}" -f $FoundSigningCerts.Count)
								if ($LdapBoschSmcSigningCert.Count -ne 0) {
									
										$FoundSigningCerts = $FoundSigningCerts + $LdapBoschSmcSigningCert
										Write-Host ("Count of LdapBoschSmcSigningCert is :" + $FoundSigningCerts.ToString())
								}
										
								if ($LdapBoschSmcSigningCert.Count -ne 0) {
								 	# $FoundCert = $LdapBoschSmcSigningCert
								 	[string]$CertFound = "SMC"
								}
								elseif ($LdapBoschSigningCert.Count -ne 0) {
									# $FoundCert = $LdapBoschSigningCert
									[string]$CertFound = "Signing"
								}
								else {
									# $FoundCert = $LdapBoschEncryptionCert
									[string]$CertFound = "Encryption"
								}

								if ($FoundSigningCerts.Count -ne 0) {
									Write-Host ("{0} certificate found in LDAP" -f $CertFound)
									foreach ($FoundCert in $FoundSigningCerts) {
										$CertSanExt = ($FoundCert.Extensions | ?{$_.Oid.Value -eq "2.5.29.17"})
										$SanObjs = New-Object -ComObject X509Enrollment.CX509ExtensionAlternativeNames -ErrorAction Stop
										[void]$SanObjs.InitializeDecode(1,[System.Convert]::ToBase64String($CertSanExt.RawData))
											foreach ($San in $SanObjs.AlternativeNames) {
												if ($San.Type -eq 11) {
													[string]$SanUpn = $San.strValue
													Write-Host ("UPN from X509 is: {0}`r`n" -f $SanUpn)
												}
												if ($San.Type -eq 2) {
													[string]$SanMail = $San.strValue
												}
											}
										$intRC = [System.Runtime.Interopservices.Marshal]::ReleaseComObject($SanObjs)
									}
								}
								else {
									# During Entrust policy change no validation possible because only a S/MIME encryption certificate is avaible.
									# Validation can be done before the policy is switched or after a successful Entrust client login and policy update.
									Write-Host ("Entrust account has no SMC- or S/MIME certificate.`r`nPlease verify your Entrust client account for current user: {0}`r`nExit validation!" -f $CurrentUpn)
									Exit
								}
								
								Clear-Variable FoundSigningCerts -ErrorAction SilentlyContinue
			
								# Start validation process
								# Validate DN and UPN
								[string]$X509DistinguishedName = ($LdapSmimeBoschCerts[0].Subject.ToString().Replace(' ',''))
								
								if ($SanUpn -eq $CurrentUpn) {
									# Write-Host ("Valid UPN match found.")
									[bool]$UpnMatch = 1
								}
								else {
									# Write-Host ("Invalid or no certificate with UPN found.")
									[bool]$UpnMatch = 0
								}
								
								if ($LdapDistinguishedName -eq $X509DistinguishedName) {
									# Write-Host ("Valid DistinguishedName match found.")
									[bool]$DnMatch = 1
								}
								else {
									# Write-Host ("Invalid DistinguishedName match found.")
									[bool]$DnMatch = 0
								}
								
								if ($SanMail.Length -ne 0) {
									if ($SanMail -eq $LdapMail) {
										# Write-Host ("Valid Email match found.")
										[bool]$MailMatch = 1
									}
									else {
										# Write-Host ("Invalid Email match found.")
										[bool]$MailMatch = 0
									}
								}
								else {
									Write-Host ("Invalid or no certificate with Mail found.")
								}

								if (($UpnMatch -eq $true) -and ($DnMatch -eq $true)) {
									[string]$Message = ("Certificate validation for current user succeeded via UPN match!`r`nDistinguishedName: {0}`r`nUPN: {1}" -f $X509DistinguishedName, $CurrentUpn)
									# Write-Host $Message
									Write-EventLog -LogName $EvtxLogName -Source "Application" -Message $Message -EventId 10 -EntryType "Information" -Category 0 -ErrorAction SilentlyContinue 
                                    $IsCertValid = 1
                                    $DistinguishedName = $X509DistinguishedName
								}
								elseif (($MailMatch -eq $true) -and ($DnMatch -eq $true)) {
									[string]$Message = ("Certificate validation for current user succeeded via Mail match!`r`nDistinguishedName: {0}`r`nMail: {1}" -f $X509DistinguishedName, $SanMail)
									# Write-Host $Message
									Write-EventLog -LogName $EvtxLogName -Source "Application" -Message $Message -EventId 10 -EntryType "Information" -Category 0 -ErrorAction SilentlyContinue 
									$IsCertValid = 2
									$DistinguishedName = $X509DistinguishedName
								}
								else {
									[string]$Message = ("Certificate validation for current user with UPN: {0} failed!`r`nDistinguishedName match: {1}`r`nUPN match: {2}`r`nLDAP DistinguishedName: {3}`r`nX.509 DistinguishedName: {4}`r`nAD UPN: {5}`r`nX.509 UPN: {6}`r`nAD Mail: {7}`r`nX.509 Mail: {8}" -f $CurrentUpn, $DnMatch, $UpnMatch, $LdapDistinguishedName, $X509DistinguishedName, $SigningSanUpn, $CurrentUpn, $LdapMail, $SanMail)
									# Write-Host $Message
									Write-EventLog -LogName $EvtxLogName -Source "Application" -Message $Message -EventId 10 -EntryType "Warning" -Category 0 -ErrorAction SilentlyContinue 
                                    $CertInvalidMessage = [string]::Format("Certificate validation for current user with UPN: {0} failed!`r`nDistinguishedName match: {1}`r`nUPN match: {2}`r`nLDAP DistinguishedName: {3}`r`nX.509 DistinguishedName: {4}`r`nAD UPN: {5}`r`nX.509 UPN: {6}`r`nAD Mail: {7}`r`nX.509 Mail: {8}", $CurrentUpn, $DnMatch, $UpnMatch, $LdapDistinguishedName, $X509DistinguishedName, $SigningSanUpn, $CurrentUpn, $LdapMail, $SanMail)
								}
							}
							else {
								#Write-Host ("No user information found in PKI LDAP for UPN: {0}" -f $CurrentUpn)
                                $CertInvalidMessage = [string]::Format("No user information found in PKI LDAP for UPN: {0}", $CurrentUpn)
							}
						}
						catch {
							[string]$ErrorMessage = $_.Exception.Message
							#Write-Host ("Something went wrong with PKI LDAP connection for`r`nUPN: {0}`r`nerror: {1}" -f $CurrentUpn.ToString(), $ErrorMessage)
                            $CertInvalidMessage = [string]::Format("Something went wrong with PKI LDAP connection for`r`nUPN: {0}`r`nerror: {1}", $CurrentUpn.ToString(), $ErrorMessage)
						}
						finally {
							[void]$CertCol.Dispose()
							[void]$LdapConnection.Dispose()
							Clear-Variable LdapValidCerts -ErrorAction SilentlyContinue
							Clear-Variable LdapValidBoschCerts -ErrorAction SilentlyContinue
							Clear-Variable LdapSmimeBoschCerts -ErrorAction SilentlyContinue
							Clear-Variable LdapBoschEncryptionCert -ErrorAction SilentlyContinue
							Clear-Variable LdapBoschSigningCert -ErrorAction SilentlyContinue
							Clear-Variable LdapBoschSmcSigningCert -ErrorAction SilentlyContinue
							Clear-Variable FoundCert -ErrorAction SilentlyContinue
							Clear-Variable CertFound -ErrorAction SilentlyContinue
							Clear-Variable SanUpn -ErrorAction SilentlyContinue
							Clear-Variable SanMail -ErrorAction SilentlyContinue
						}
					}
					else {
						#Write-Host ("Did not get current UPN from BCN ActiveDirectory.")
                        $CertInvalidMessage = "Did not get current UPN from BCN ActiveDirectory."
					}
				}
				catch {
					[string]$ErrorMessage = $_.Exception.Message
					#Write-Host ("Somemthing went wrong with BCN ActiveDirectory connect with error: {0}" -f $ErrorMessage)
                    $CertInvalidMessage = [string]::Format("Somemthing went wrong with BCN ActiveDirectory connect with UPN Name: {0}", $CurrentUpn)
				}
			}
			else {
				#Write-Host ("No valid Signing Certs. found. Please start Entrust client.") 
                $CertInvalidMessage = "No valid Signing Certs. found. Please start Entrust client."
			}
		}
	#}
	#else {
		#Write-Host ("No Certs. in local certificate store found for local user: {0}" -f ($Env:USERDOMAIN.ToString() + "\" + $Env:USERNAME.ToString())) 
       # $CertInvalidMessage = [string]::Format("No Certs. in local certificate store found for local user: {0}", ($Env:USERDOMAIN.ToString() + "\" + $Env:USERNAME.ToString()))
	#}
}
catch{
	[string]$ErrorMessage = $_.Exception.Message
	#Write-Host ("Can not open user certificate store with error: {0}" -f $ErrorMessage)
    $CertInvalidMessage = "Can not open user certificate store on Certificates Manager, Please contact CI Hotline to get support"
}
finally {
	# Write-Host ("Variable clean-up")
    Write-Output ($IsCertValid)
    Write-Output ($CertInvalidMessage)
    Write-Output ($DistinguishedName)
	[void]$CertStore.Close()
	[void]$CertStore.Dispose()
	Clear-Variable ValidCerts -ErrorAction SilentlyContinue
	Clear-Variable ValidBoschCerts -ErrorAction SilentlyContinue
	Clear-Variable SmimeBoschCerts -ErrorAction SilentlyContinue
	Clear-Variable SmimeEncryptionCert -ErrorAction SilentlyContinue
	Clear-Variable SmimeSigningCert -ErrorAction SilentlyContinue
	Clear-Variable SmcSigningCerts -ErrorAction SilentlyContinue
	Clear-Variable SmcSigningUserCert -ErrorAction SilentlyContinue
	Clear-Variable DistinguishedName -ErrorAction SilentlyContinue
}
}




certValidater $ntid
