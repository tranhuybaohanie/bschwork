Build Project: TwoFactorAuthenticationInstaller
PRO:
<add key="WcfServiceBaseUrlServer" value="https://rb-2fa.de.bosch.com/2fa-service/ServiceContracts/" />
<add key="BoschCA1DeDn" value="CN=Bosch-CA1-DE, CN=PKI, DC=Bosch, DC=com" />
<add key="PKIDN" value="cn=bosch,cn=pki,dc=bosch,dc=com" />
<add key="TrustService" value="https://fr2VM178.de.bosch.com:443/" />
<add key="TCIName" value="APCWSClient_P" />
<add key="TCICode" value="rc_65H!m1lRe$LN5dyaS" />

DEV-Testing:
<add key="WcfServiceBaseUrlServer" value="https://rb-2fa-dev.de.bosch.com/2fa-service/ServiceContracts/" />
<add key="BoschCA1DeDn" value="CN=Bosch-CA1-DE, CN=PKI, DC=Bosch, DC=com" />
<add key="PKIDN" value="cn=bosch,cn=pki,dc=bosch,dc=com" />
<add key="TrustService" value="https://fr2VM178.de.bosch.com:443/" />
<add key="TCIName" value="APCWSClient_P" />
<add key="TCICode" value=" rc_65H!m1lRe$LN5dyaS" />

Q9:
<add key="WcfServiceBaseUrlServer" value="http://fe0vm02840.q9-bcd-de.q9-bcd-bosch.test-bosch.com/2fa-service/ServiceContracts/" />
<add key="BoschCA1DeDn" value="CN=BoschCA1-DE, CN=PKI, DC=Bosch, DC=com" />
<add key="PKIDN" value="test-bosch,cn=pki,dc=bosch,dc=com" />
<add key="pkiLdapBaseDN" value="cn=test-bosch,cn=pki,dc=bosch,dc=com" />
<add key="TrustService" value="https://fr2vm172.de.bosch.com:443/" />
<add key="TCIName" value="APCWSClient_Q" />
<add key="TCICode" value="2gbp3XspB4Bt2NjWgW6i" />
--------------------------------------------------------------------------
new version-----------------------
ent_bosch_3k_2fa_v1 (Q9 ent_bosch_3k_sclog2)
ent_bosch_twokey


client_Q QVBDV1NDbGllbnRfUToyZ2JwM1hzcEI0QnQyTmpXZ1c2aQ==

client_P QVBDV1NDbGllbnRfUDpyY182NUghbTFsUmUkTE41ZHlhUw== 
-----------------------------------------------------------
[?17-?May-?19 9:58 AM]  Nguyen Thanh Hien (RBVH/ETM3):  
2 account m�i tru?ng production n�: 
Test08.2FA_WinLogon@de.bosch.com	DE	AWF1FE		4Awf1Fe2uSe!	NN16OX563D7J		Harish Kumar Bindela
Test09.2FA_WinLogon@de.bosch.com	DE	ATW1FE		4Atw1Fe2uSe!	OLFSA2WODKFR		Harish Kumar Bindela 
 
